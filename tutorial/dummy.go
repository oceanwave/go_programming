package main

import (
	"fmt"
	"runtime"
	"unsafe"
)

const Pi = 3

func namedReturnVal(sum int) (x, y int) {
	/*
		返回值也是有名称的，相当于是把变量声明放到了方法签名里边
	*/

	x = sum * 2 / 3
	y = sum - x

	return
}

func theSwitch() {

	switch os:=runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X")
		//  不用加 break， golang 只匹配一个case
	case "linux":
		fmt.Println("Linux")
	default:
		fmt.Println("%s\n", os)
	}
}


func theDefer() {
	fmt.Println()
	for i := 0; i<5; i++ {
		defer fmt.Println(i)
	}

	fmt.Println("......")

	/*
		defer 只是在函数返回前，按stack 的方式执行
		而不是在作用域退出时执行
	*/
	{
		fmt.Println("before defer")
		defer fmt.Println("defer executing")
		fmt.Println("after defer")
	}

	fmt.Println("the last statement")
}

func Pointers() {
	var p *int

	i := 2

	p = &i

	fmt.Println("p=", p)

	// uintptr 的运算操作单位是1个字节
	p = (*int)(unsafe.Pointer(uintptr(unsafe.Pointer(p)) + unsafe.Sizeof(p)))

	fmt.Println("p=", p)
}

func containers(idx int) {
	var a [2]int
	fmt.Println(a[idx+1])

	var originArr [3]int = [3]int{1, 2, 3}
	fmt.Println("oiginArr:", originArr)

	var s []int = originArr[0:1]
	// s = append(s, 10, 11, 12, 13)
	s = append(s, 10)
	fmt.Println("slice s:", s)

	fmt.Println("oiginArr:", originArr)

	var sl[] int

	fmt.Println(sl)

	b := make([]int, 5)
	fmt.Println(b)
}

func other(divider int) {
	if divider == 2 && 2/(divider-1)==0 {
		fmt.Println("got here")
	}

	m := make(map[string]int)
	m["Answer"] = 42
	//elem, ok := m["f"]
	elem, ok := m["Answer"]
	fmt.Println("retrive: ", elem, ok)

}


func newInt(val int) *int {

	/*
		https://www.shangmayuan.com/a/49170039ac504cb88710bce1.html
		这样的用户在go里边是允许的
		因为数据分配在 堆 还是 栈 上是有编译器根据 escape analysis 得出来的
		并不是程序员手动指定的
		同理，即使是使用了new，变量依然可能在栈上
		escape analysis 简单理解就是变量的引用是否在当前作用域之外
	 */
	var ret int = 1
	return &ret
}

func lifetimeAndEscape()  {

	a := newInt(1)
	fmt.Println("a =", *a)

}

func main() {
	// fmt.Println(namedReturnVal(10))
	// theSwitch()
	// theDefer()
	// Pointers()
	// containers(0)
	// other(1)
	lifetimeAndEscape()
}
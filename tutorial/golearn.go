package main

import (
	"fmt"
	"github.com/sjwhitworth/golearn/base"
	"github.com/sjwhitworth/golearn/linear_models"
)

func main() {
		X, err := base.ParseCSVToInstances("train.csv", false)
		Y, err := base.ParseCSVToInstances("test.csv", false)
		lr, err := linear_models.NewLogisticRegression("l1", 1.0, 1e-6)

		lr.Fit(X)

		Z, err := lr.Predict(Y)
		fmt.Println(Z, err)

	//Convey("When predicting the label of first vector", func() {
	//
	//		So(err, ShouldEqual, nil)
	//		Convey("The result should be 1", func() {
	//			So(Z.RowString(0), ShouldEqual, "-1.0")
	//		})
	//	})
	//	Convey("When predicting the label of second vector", func() {
	//		Z, err := lr.Predict(Y)
	//		So(err, ShouldEqual, nil)
	//		Convey("The result should be -1", func() {
	//			So(Z.RowString(1), ShouldEqual, "-1.0")
	//		})
	//	})
	//	So((*lr).String(), ShouldEqual, "LogisticRegression"))
}


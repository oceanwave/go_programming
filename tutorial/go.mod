module tutorial

go 1.17

require (
	github.com/sjwhitworth/golearn v0.0.0-20211014193759-a8b69c276cd8
	gonum.org/v1/gonum v0.9.3
)

require (
	github.com/gonum/blas v0.0.0-20181208220705-f22b278b28ac // indirect
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/guptarohit/asciigraph v0.5.1 // indirect
	github.com/mattn/go-runewidth v0.0.7 // indirect
	github.com/olekukonko/tablewriter v0.0.4 // indirect
	github.com/rocketlaunchr/dataframe-go v0.0.0-20201007021539-67b046771f0b // indirect
	github.com/smartystreets/goconvey v1.6.7 // indirect
	golang.org/x/exp v0.0.0-20200331195152-e8c3332aa8e5 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
)

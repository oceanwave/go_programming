package main

import (
	"fmt"
	"strconv"
)

type Element interface{}
type list [] Element

type Person struct {
	name string
	age int
}

func (p Person) String() string {
	return "name: " + p.name + ", age:" + strconv.Itoa(p.age);
}

func main() {
	list := make([] interface{}, 3)
	list[0] = 1
	list[1] = "Hello"
	list[2] = Person{"yanan", 19}

	for index, element := range list {
		switch value := element.(type) {
		case int:
			fmt.Printf("list[%d] is and int and value is %d\n", index, value)
		default:
			fmt.Printf("list[%d] is of a different type\n", index)
		}
	}
}
